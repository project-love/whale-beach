function love.conf(t)
    t.version = "0.10.1"
    t.window.title = "Whale Beach"
    t.window.width = 320
    t.window.height = 240

    t.modules.audio = false
    t.modules.event = true
    t.modules.graphics = true
    t.modules.image = true
    t.modules.joystick = false
    t.modules.keyboard = true
    t.modules.math = false
    t.modules.mouse = false
    t.modules.physics = false
    t.modules.sound = false
    t.modules.system = false
    t.modules.timer = true
    t.modules.touch = false
    t.modules.video = false
    t.modules.window = true
    t.modules.thread = false

    t.releases = {
      title = 'WhaleBeach',
      package = 'whale-beach',
      loveVersion = '0.10.1',
      author = 'James D. Marble',
      email = 'james.d.marble@gmail.com',
      description = 'a clone of Pong',
      homepage = 'https://gitlab.com/jdmarble/whale-beach',
      identifier = 'com.gitlab.jdmarble.whale-beach',
    }

end
