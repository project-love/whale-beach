# Whale Beach

A clone of Pong using [LÖVE](https://love2d.org/).
See a live demo at https://whale-beach.jdmarble.pw.

Downloads:

  * [WhaleBeach.love](http://jdmarble.gitlab.io/whale-beach/WhaleBeach.love)
  * [Windows executable (32-bit)](http://jdmarble.gitlab.io/whale-beach/WhaleBeach-win32.zip)
  * [Windows executable (64-bit)](http://jdmarble.gitlab.io/whale-beach/WhaleBeach-win64.zip)
  * [Mac OSX](http://jdmarble.gitlab.io/whale-beach/WhaleBeach-macosx.zip)

## Building

To avoid the nuisance of configuring the build environment, you should use Docker:

    docker run --rm --tty --interactive \
               --volume=`pwd`:/workspace --workdir=/workspace --env="HOME=/workspace" \
               --user=`id --user`:`id --group` \
               jdmarble/project-love:1.2.0 ./build.sh
