-- Sets the 'size' values so the 'img' is scaled so pixels are exact for the current screen size
function setScale(thing)
  local scaleFactor = love.graphics.getHeight() / 2
  thing.x.size = thing.img:getWidth() / scaleFactor
  thing.y.size = thing.img:getHeight() / scaleFactor
  return thing
end

function love.load()
  if arg and arg[#arg] == "-debug" then
    require("mobdebug").start()
  end
  
  local buoyImg = love.graphics.newImage('buoy.png')
  local beachImg = love.graphics.newImage('beach.png')
  things = {
    leftPaddle = setScale({
      img = buoyImg,
      x = {pos = -1},
      y = {pos = 0, downKey = 's', upKey = 'w'}}),
    leftBeach = setScale({
      img = beachImg,
      x = {pos = -1.3},
      y = {pos = 0}}),
    rightPaddle = setScale({
      img = buoyImg,
      x = {pos = 1},
      y = {pos = 0, downKey = 'down', upKey = 'up'}}),
    rightBeach = setScale({
      img = beachImg, flipx = true,
      x = {pos = 1.3},
      y = {pos = 0}}),
    ball = setScale({
      img = love.graphics.newImage('whale.png'),
      x = {pos = 0, vel = -0.5},
      y = {pos = 0, vel = 1}}),
    topWall = {
      x = {pos = 0, size = 4},
      y = {pos = -2, size = 2}},
    bottomWall = {
      x = {pos = 0, size = 4},
      y = {pos = 2, size = 2}}}
end

function love.draw()
  love.graphics.setBackgroundColor(174, 231, 255)
  love.graphics.translate(
    love.graphics.getWidth() / 2,
    love.graphics.getHeight() / 2)
  love.graphics.scale(love.graphics.getHeight() / 2)

  for i, thing in pairs(things) do
    if thing.img then
      local scale_x = thing.x.size / thing.img:getWidth()
      local scale_y = thing.y.size / thing.img:getHeight()
      -- find the upper left corner of the box from the center and size
      local corner_x = thing.x.pos - thing.x.size/2
      local corner_y = thing.y.pos - thing.y.size/2
      local angleRads = 0
      if thing.flipx then
        scale_x = -scale_x
        corner_x = corner_x + thing.x.size
      end
      love.graphics.draw(thing.img, corner_x, corner_y, angleRads, scale_x, scale_y)
    end
  end
end

local function handle_input(dim)
  if dim.upKey or dim.downKey then
    if love.keyboard.isDown(dim.upKey) then
      dim.vel = -1
    elseif love.keyboard.isDown(dim.downKey) then
      dim.vel = 1
    else
      dim.vel = nil
    end
  end
end

local function move(dim, dt)
  if dim.vel then
    dim.pos = dim.pos + dim.vel*dt
  end
end

-- Single dimensional collision detection
local function is_colliding_1d(a1, a2)
  local low1 = a1.pos - a1.size/2
  local low2 = a2.pos - a2.size/2
  local hi1 = a1.pos + a1.size/2
  local hi2 = a2.pos + a2.size/2

  if low1 < low2 then
    return hi1 > low2
  else
    return hi2 > low1
  end
end

local function collision_time(a1, a2)
  local collision_time = -math.huge
  if a1.vel and a1.vel ~= 0 then
    local collision_pos
    if (a1.vel > 0) then
      collision_pos = (a2.pos - a2.size/2) - (a1.pos + a1.size/2)
    else
      collision_pos = (a2.pos + a2.size/2) - (a1.pos - a1.size/2)
    end
    collision_time = collision_pos / a1.vel
  end
  return collision_time
end

local function collision_response(a, time)
    move(a, time) -- rewind until collision
    a.vel = -a.vel -- reverse direction
    move(a, -time) -- apply new direction
end

local function collide_and_respond(x1, y1, x2, y2, dt)
  if not is_colliding_1d(x1, x2) or not is_colliding_1d(y1, y2) then
    -- No collision
    return
  end
  
  local x_collision_time = collision_time(x1, x2)
  local y_collision_time = collision_time(y1, y2)
  
  if x_collision_time > y_collision_time then
    collision_response(x1, x_collision_time)
  else
    collision_response(y1, y_collision_time)
  end
end

function love.update(dt)
  for i, thing in pairs(things) do

    -- Accelerate paddles
    handle_input(thing.x)
    handle_input(thing.y)
    
    -- Move things with velocity
    move(thing.x, dt)
    move(thing.y, dt)
    
    -- Collision detection and response
    if thing.x.vel or thing.y.vel then
      for j, other in pairs(things) do
        if i ~= j then
          collide_and_respond(thing.x, thing.y, other.x, other.y, dt)
        end
      end
    end
  end
end
